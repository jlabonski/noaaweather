NOAA data communication
=======================

Primary forecast is from:

    http://mobile.weather.gov/wtf/MapClick.php?rand=6524.003641089641&lat=39.88&lon=-75.23&FcstType=json

and returns the following JSON document:

    {
        "operationalMode":"",
        "srsName":"WGS 1984",
        "creationDate":"2014-04-15T09:33:41-04:00",
        "creationDateLocal":"15 Apr 11:54 am EDT",
        "productionCenter":"Philadelphia, PA",
        "credit":"http://www.erh.noaa.gov/phi/",
        "moreInformation":"http://weather.gov",
        "location":{
            "latitude":"39.88",
            "longitude":"-75.23",
            "elevation":"3",
            "wfo":"PHI",
            "timezone":"E|Y|5",
            "areaDescription":"Philadelphia International Airport PA",
            "radar":"kdix",
            "zone":"PAZ071",
            "county":"PAC101",
            "firezone":"PAZ071",
            "metar":"KPHL"},
        "time":{
            "layoutKey":"k-p12h-n13-1",
            "startPeriodName":[
                "This Afternoon",
                "Tonight",
                "Wednesday",
                "Wednesday Night",
                "Thursday",
                "Thursday Night",
                "Friday",
                "Friday Night",
                "Saturday",
                "Saturday Night",
                "Sunday",
                "Sunday Night",
                "Monday"
            ],
            "startValidTime":[
                "2014-04-15T12:00:00-04:00",
                "2014-04-15T18:00:00-04:00",
                "2014-04-16T06:00:00-04:00",
                "2014-04-16T18:00:00-04:00",
                "2014-04-17T06:00:00-04:00",
                "2014-04-17T18:00:00-04:00",
                "2014-04-18T06:00:00-04:00",
                "2014-04-18T18:00:00-04:00",
                "2014-04-19T06:00:00-04:00",
                "2014-04-19T18:00:00-04:00",
                "2014-04-20T06:00:00-04:00",
                "2014-04-20T18:00:00-04:00",
                "2014-04-21T06:00:00-04:00"
            ],
            "tempLabel":["High","Low","High","Low","High","Low","High","Low","High","Low","High","Low","High"]
        },
        "data":{
            "temperature":["69","30","48","35","55","36","57","42","60","43","61","44","62"],
            "pop":["100","90",null,null,null,null,null,null,"40",null,null,null,"30"],

            "weather":[
                "Heavy Rain",
                "Rain/Snow",
                "Breezy",
                "Mostly Clear",
                "Sunny",
                "Partly Cloudy",
                "Partly Sunny",
                "Mostly Cloudy",
                "Chance Showers",
                "Mostly Cloudy",
                "Mostly Sunny",
                "Partly Cloudy",
                "Chance Showers"
                ],
            "iconLink":[
                "http://forecast.weather.gov/images/wtf/medium/shra100.png",
                "http://forecast.weather.gov/images/wtf/medium/nrasn90.png",
                "http://forecast.weather.gov/images/wtf/medium/wind.png",
                "http://forecast.weather.gov/images/wtf/medium/nfew.png",
                "http://forecast.weather.gov/images/wtf/medium/few.png",
                "http://forecast.weather.gov/images/wtf/medium/nsct.png",
                "http://forecast.weather.gov/images/wtf/medium/bkn.png",
                "http://forecast.weather.gov/images/wtf/medium/nbkn.png",
                "http://forecast.weather.gov/images/wtf/medium/shra40.png",
                "http://forecast.weather.gov/images/wtf/medium/nbkn.png",
                "http://forecast.weather.gov/images/wtf/medium/sct.png",
                "http://forecast.weather.gov/images/wtf/medium/nsct.png",
                "http://forecast.weather.gov/images/wtf/medium/shra30.png"
                ],
            "hazard":[
                "Freeze Warning",			"Hazardous Weather Outlook",			"Short Term Forecast"
                ],
            "hazardUrl":[
                "http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Freeze+Warning",			"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Hazardous+Weather+Outlook",			"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Short+Term+Forecast"
                ],
            "text":[
                "Showers and possibly a thunderstorm. Some of the storms could produce heavy rainfall.  High near 69. Breezy, with a southwest wind around 21 mph, with gusts as high as 31 mph.  Chance of precipitation is 100%. New rainfall amounts between a half and three quarters of an inch possible. ",
                "Rain showers, possibly mixing with snow after 1am, then gradually ending. Some thunder is also possible.  Low around 30. Blustery, with a northwest wind 21 to 24 mph, with gusts as high as 40 mph.  Chance of precipitation is 90%. Little or no snow accumulation expected. ",
                "Sunny, with a high near 48. Breezy, with a north wind 13 to 21 mph. ",
                "Mostly clear, with a low around 35. Northeast wind 6 to 8 mph. ",
                "Sunny, with a high near 55. East wind around 8 mph. ",
                "Partly cloudy, with a low around 36.",
                "Partly sunny, with a high near 57.",
                "Mostly cloudy, with a low around 42.",
                "A chance of showers.  Mostly cloudy, with a high near 60. Chance of precipitation is 40%.",
                "Mostly cloudy, with a low around 43.",
                "Mostly sunny, with a high near 61.",
                "Partly cloudy, with a low around 44.",
                "A chance of showers.  Partly sunny, with a high near 62. Chance of precipitation is 30%."			]
            },
            "currentobservation":{
                "id":"KPHL",
                "name":"Philadelphia International Airport",
                "elev":"5",
                "latitude":"39.87",
                "longitude":"-75.23",
                "Date":"15 Apr 11:54 am EDT",
                "Temp":"66",
                "Dewp":"61",
                "Relh":"84",
                "Winds":"14",
                "Windd":"170",
                "Gust":"0",
                "Weather":" Light Rain",
                "Weatherimage":"ra.png",
                "Visibility":"5.00",
                "Altimeter":"1001.7",
                "SLP":"29.58",
                "timezone":"EDT",
                "state":"PA",
                "WindChill":"66"
            }
        }
    }


Secondary products are here:

    http://mobile.weather.gov/prodDBQuery.php?nnn=AFD&xxx=PHI&format=2

And return 