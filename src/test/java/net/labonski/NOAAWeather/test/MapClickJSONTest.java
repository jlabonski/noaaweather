package net.labonski.NOAAWeather.test;

import com.google.gson.Gson;
import net.labonski.NOAAWeather.gson.GSONFactory;
import net.labonski.NOAAWeather.model.Forecast;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jlabonski on 6/24/2014.
 */
@RunWith(RobolectricTestRunner.class)
public class MapClickJSONTest
{

    @Test
    public void testJSONDeserialization1()
    {
        Gson g = GSONFactory.get();

        Forecast f = g.fromJson(test1, Forecast.class);

        assertEquals(f.credit, "http://www.erh.noaa.gov/phi/");
	    assertEquals(f.productionCenter, "Philadelphia, PA");
	    assertEquals(f.creationDate, new DateTime("2014-06-24T10:27:11-04:00"));
		assertEquals(f.moreInformation, "http://weather.gov");
	    assertNotNull(f.location);
	    assertEquals(f.location.latitude, 39.88, 0.001f);
	    assertEquals(f.location.longitude, -75.23, 0.001f);
	    assertEquals(f.location.elevation, 3);
	    assertEquals(f.location.wfo, "PHI");
	    assertNotNull(f.periods);
	    assertEquals(f.periods.size(), 13);

	    assertEquals(f.periods.get(0).name, "This Afternoon");
	    assertEquals(f.periods.get(0).icon, "http://forecast.weather.gov/images/wtf/medium/bkn.png");
	    assertEquals(f.periods.get(0).tempLabel, "High");
	    assertEquals(f.periods.get(0).temp, 86);
	    assertEquals(f.periods.get(0).text, "Partly sunny, with a high near 86. South wind around 10 mph.");
	    assertEquals(f.periods.get(0).weather, "Partly Sunny");
	    assertEquals(f.periods.get(0).pop, 0);
	    assertEquals(f.periods.get(0).startTime, new DateTime("2014-06-24T12:00:00-04:00"));

	    assertEquals(f.observation.id, "KPHL");
	    assertEquals(f.observation.name, "Philadelphia International Airport");
	    assertEquals(f.observation.elevation, 5);
	    assertEquals(f.observation.latitude, 39.87, 0.01f);
	    assertEquals(f.observation.longitude, -75.23, 0.01f);
	    assertEquals(f.observation.date, new DateTime("2014-06-24T10:54:00-04:00"));
	    assertEquals(f.observation.temp, 79);
	    assertEquals(f.observation.dewpoint, 52);
	    assertEquals(f.observation.relativeHumidity, 39);
	    assertEquals(f.observation.windSpeed, 12);
	    assertEquals(f.observation.windDirection, 190);
	    assertEquals(f.observation.gust, 0);
	    assertEquals(f.observation.weather, "Mostly Cloudy");
	    assertEquals(f.observation.weatherImage, "bkn.png");
	    assertEquals(f.observation.visibility, 10.00, 0.01f);
	    assertEquals(f.observation.altimeter, 1020.9, 0.1f);
	    assertEquals(f.observation.SLP, 30.15, 0.01f);
	    assertEquals(f.observation.timezone, "EDT");
	    assertEquals(f.observation.state, "PA");
	    assertEquals(f.observation.windchill, 82);


    }

	@Test
	public void testJSONDeserialization2()
	{
		Gson g = GSONFactory.get();

		Forecast f = g.fromJson(test2, Forecast.class);

		assertEquals(f.credit, "http://www.erh.noaa.gov/phi/");
		assertEquals(f.productionCenter, "Philadelphia, PA");
		assertEquals(f.creationDate, new DateTime("2014-04-15T09:33:41-04:00"));
		assertEquals(f.moreInformation, "http://weather.gov");
		assertNotNull(f.location);
		assertEquals(f.location.latitude, 39.88, 0.001f);
		assertEquals(f.location.longitude, -75.23, 0.001f);
		assertEquals(f.location.elevation, 3);
		assertEquals(f.location.wfo, "PHI");
		assertNotNull(f.periods);
		assertEquals(f.periods.size(), 13);

		assertEquals(f.periods.get(0).name, "This Afternoon");
		assertEquals(f.periods.get(0).icon, "http://forecast.weather.gov/images/wtf/medium/shra100.png");
		assertEquals(f.periods.get(0).tempLabel, "High");
		assertEquals(f.periods.get(0).temp, 69);
		assertEquals(f.periods.get(0).text, "Showers and possibly a thunderstorm. Some of the storms could produce heavy rainfall.  High near 69. Breezy, with a southwest wind around 21 mph, with gusts as high as 31 mph.  Chance of precipitation is 100%. New rainfall amounts between a half and three quarters of an inch possible.");
		assertEquals(f.periods.get(0).weather, "Heavy Rain");
		assertEquals(f.periods.get(0).pop, 100);
		assertEquals(f.periods.get(0).startTime, new DateTime("2014-04-15T12:00:00-04:00"));

		assertEquals(f.observation.id, "KPHL");
		assertEquals(f.observation.name, "Philadelphia International Airport");
		assertEquals(f.observation.elevation, 5);
		assertEquals(f.observation.latitude, 39.87, 0.01f);
		assertEquals(f.observation.longitude, -75.23, 0.01f);
		assertEquals(f.observation.date, new DateTime("2014-04-15T11:54:00-04:00"));
		assertEquals(f.observation.temp, 66);
		assertEquals(f.observation.dewpoint, 61);
		assertEquals(f.observation.relativeHumidity, 84);
		assertEquals(f.observation.windSpeed, 14);
		assertEquals(f.observation.windDirection, 170);
		assertEquals(f.observation.gust, 0);
		assertEquals(f.observation.weather, "Light Rain");
		assertEquals(f.observation.weatherImage, "ra.png");
		assertEquals(f.observation.visibility, 5.00, 0.01f);
		assertEquals(f.observation.altimeter, 1001.7, 0.1f);
		assertEquals(f.observation.SLP, 29.58, 0.01f);
		assertEquals(f.observation.timezone, "EDT");
		assertEquals(f.observation.state, "PA");
		assertEquals(f.observation.windchill, 66);




	}


	String test1 = "{\n" +
			"\t\"operationalMode\":\"Production\",\n" +
			"\t\"srsName\":\"WGS 1984\",\n" +
			"\t\"creationDate\":\"2014-06-24T10:27:11-04:00\",\n" +
			"\t\"creationDateLocal\":\"24 Jun 10:54 am EDT\",\n" +
			"\t\"productionCenter\":\"Philadelphia, PA\",\n" +
			"\t\"credit\":\"http://www.erh.noaa.gov/phi/\",\n" +
			"\t\"moreInformation\":\"http://weather.gov\",\n" +
			"\t\"location\":{\n" +
			"\t\t\"latitude\":\"39.88\",\n" +
			"\t\t\"longitude\":\"-75.23\",\n" +
			"\t\t\"elevation\":\"3\",\n" +
			"\t\t\"wfo\":\"PHI\",\n" +
			"\t\t\"timezone\":\"E|Y|5\",\n" +
			"\t\t\"areaDescription\":\"Philadelphia International Airport PA\",\n" +
			"\t\t\"radar\":\"kdix\",\n" +
			"\t\t\"zone\":\"PAZ071\",\n" +
			"\t\t\"county\":\"PAC101\",\n" +
			"\t\t\"firezone\":\"PAZ071\",\n" +
			"\t\t\"metar\":\"KPHL\"\n" +
			"},\n" +
			"\t\"time\":{\n" +
			"\t\t\"layoutKey\":\"k-p12h-n13-1\",\n" +
			"\t\t\"startPeriodName\":[\n" +
			"\t\t\t\"This Afternoon\",\n" +
			"\t\t\t\"Tonight\",\n" +
			"\t\t\t\"Wednesday\",\n" +
			"\t\t\t\"Wednesday Night\",\n" +
			"\t\t\t\"Thursday\",\n" +
			"\t\t\t\"Thursday Night\",\n" +
			"\t\t\t\"Friday\",\n" +
			"\t\t\t\"Friday Night\",\n" +
			"\t\t\t\"Saturday\",\n" +
			"\t\t\t\"Saturday Night\",\n" +
			"\t\t\t\"Sunday\",\n" +
			"\t\t\t\"Sunday Night\",\n" +
			"\t\t\t\"Monday\"\t\t\n" +
			"\t\t],\n" +
			"\t\t\"startValidTime\":[\n" +
			"\t\t\t\"2014-06-24T12:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-24T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-25T06:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-25T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-26T06:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-26T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-27T06:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-27T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-28T06:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-28T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-29T06:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-29T18:00:00-04:00\",\n" +
			"\t\t\t\"2014-06-30T06:00:00-04:00\"\t\n" +
			"\t\t],\n" +
			"\t\t\"tempLabel\":[\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\"]\n" +
			"\t},\n" +
			"\t\"data\":{\n" +
			"\t\t\"temperature\":[\"86\",\"70\",\"86\",\"70\",\"86\",\"69\",\"84\",\"66\",\"83\",\"67\",\"86\",\"68\",\"87\"],\t\t\n" +
			"\t\t\"pop\":[null,null,\"30\",\"70\",\"30\",null,null,null,null,null,null,null,null],\n" +
			"\n" +
			"\t\t\"weather\":[\n" +
			"\t\t\t\"Partly Sunny\",\n" +
			"\t\t\t\"Mostly Cloudy\",\n" +
			"\t\t\t\"Scattered Thunderstorms\",\n" +
			"\t\t\t\"Heavy Rain\",\n" +
			"\t\t\t\"Scattered Thunderstorms\",\n" +
			"\t\t\t\"Mostly Cloudy\",\n" +
			"\t\t\t\"Partly Sunny\",\n" +
			"\t\t\t\"Mostly Cloudy\",\n" +
			"\t\t\t\"Partly Sunny\",\n" +
			"\t\t\t\"Partly Cloudy\",\n" +
			"\t\t\t\"Partly Sunny\",\n" +
			"\t\t\t\"Partly Cloudy\",\n" +
			"\t\t\t\"Mostly Sunny\"\n" +
			"\t\t\t],\n" +
			"\t\t\"iconLink\":[\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/bkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nbkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/tsra30.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/ntsra70.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/tsra30.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nbkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/bkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nbkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/bkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nsct.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/bkn.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nsct.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/sct.png\"\n" +
			"\t\t\t],\n" +
			"\t\t\"hazard\":[\n" +
			"\t\t\t\"Hazardous Weather Outlook\"\n" +
			"\t\t\t],\n" +
			"\t\t\"hazardUrl\":[\n" +
			"\t\t\t\"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Hazardous+Weather+Outlook\"\n" +
			"\t\t\t],\n" +
			"\t\t\"text\":[\n" +
			"\t\t\t\"Partly sunny, with a high near 86. South wind around 10 mph. \",\n" +
			"\t\t\t\"Mostly cloudy, with a low around 70. South wind 5 to 9 mph becoming light  after midnight. \",\n" +
			"\t\t\t\"Scattered showers and thunderstorms after noon.  Mostly cloudy, with a high near 86. South wind 6 to 11 mph.  Chance of precipitation is 30%. New rainfall amounts of less than a tenth of an inch, except higher amounts possible in thunderstorms. \",\n" +
			"\t\t\t\"Showers and thunderstorms likely, mainly before midnight. Some of the storms could produce heavy rainfall.  Cloudy, with a low around 70. South wind 5 to 9 mph becoming calm  after midnight.  Chance of precipitation is 70%. New rainfall amounts between a quarter and half of an inch possible. \",\n" +
			"\t\t\t\"Scattered showers and thunderstorms, mainly before 8am.  Mostly cloudy, with a high near 86. Northwest wind 5 to 9 mph.  Chance of precipitation is 30%. New rainfall amounts between a tenth and quarter of an inch, except higher amounts possible in thunderstorms. \",\n" +
			"\t\t\t\"Mostly cloudy, with a low around 69.\",\n" +
			"\t\t\t\"Partly sunny, with a high near 84.\",\n" +
			"\t\t\t\"Mostly cloudy, with a low around 66.\",\n" +
			"\t\t\t\"Partly sunny, with a high near 83.\",\n" +
			"\t\t\t\"Partly cloudy, with a low around 67.\",\n" +
			"\t\t\t\"Partly sunny, with a high near 86.\",\n" +
			"\t\t\t\"Partly cloudy, with a low around 68.\",\n" +
			"\t\t\t\"Mostly sunny, with a high near 87.\"\t\t\t]\n" +
			"\t\t},\n" +
			"\t\t\"currentobservation\":{\n" +
			"\t\t\t\"id\":\"KPHL\",\n" +
			"\t\t\t\"name\":\"Philadelphia International Airport\",\n" +
			"\t\t\t\"elev\":\"5\",\n" +
			"\t\t\t\"latitude\":\"39.87\",\n" +
			"\t\t\t\"longitude\":\"-75.23\",\n" +
			"\t\t\t\"Date\":\"24 Jun 10:54 am EDT\",\n" +
			"\t\t\t\"Temp\":\"79\",\n" +
			"\t\t\t\"Dewp\":\"52\",\n" +
			"\t\t\t\"Relh\":\"39\",\n" +
			"\t\t\t\"Winds\":\"12\",\n" +
			"\t\t\t\"Windd\":\"190\",\n" +
			"\t\t\t\"Gust\":\"0\",\n" +
			"\t\t\t\"Weather\":\"Mostly Cloudy\",\n" +
			"\t\t\t\"Weatherimage\":\"bkn.png\",\n" +
			"\t\t\t\"Visibility\":\"10.00\",\n" +
			"\t\t\t\"Altimeter\":\"1020.9\",\n" +
			"\t\t\t\"SLP\":\"30.15\",\n" +
			"\t\t\t\"timezone\":\"EDT\",\n" +
			"\t\t\t\"state\":\"PA\",\n" +
			"\t\t\t\"WindChill\":\"82\"\n" +
			"\t\t}\n" +
			"\t}";

	String test2 = "    {\n" +
			"        \"operationalMode\":\"\",\n" +
			"        \"srsName\":\"WGS 1984\",\n" +
			"        \"creationDate\":\"2014-04-15T09:33:41-04:00\",\n" +
			"        \"creationDateLocal\":\"15 Apr 11:54 am EDT\",\n" +
			"        \"productionCenter\":\"Philadelphia, PA\",\n" +
			"        \"credit\":\"http://www.erh.noaa.gov/phi/\",\n" +
			"        \"moreInformation\":\"http://weather.gov\",\n" +
			"        \"location\":{\n" +
			"            \"latitude\":\"39.88\",\n" +
			"            \"longitude\":\"-75.23\",\n" +
			"            \"elevation\":\"3\",\n" +
			"            \"wfo\":\"PHI\",\n" +
			"            \"timezone\":\"E|Y|5\",\n" +
			"            \"areaDescription\":\"Philadelphia International Airport PA\",\n" +
			"            \"radar\":\"kdix\",\n" +
			"            \"zone\":\"PAZ071\",\n" +
			"            \"county\":\"PAC101\",\n" +
			"            \"firezone\":\"PAZ071\",\n" +
			"            \"metar\":\"KPHL\"},\n" +
			"        \"time\":{\n" +
			"            \"layoutKey\":\"k-p12h-n13-1\",\n" +
			"            \"startPeriodName\":[\n" +
			"                \"This Afternoon\",\n" +
			"                \"Tonight\",\n" +
			"                \"Wednesday\",\n" +
			"                \"Wednesday Night\",\n" +
			"                \"Thursday\",\n" +
			"                \"Thursday Night\",\n" +
			"                \"Friday\",\n" +
			"                \"Friday Night\",\n" +
			"                \"Saturday\",\n" +
			"                \"Saturday Night\",\n" +
			"                \"Sunday\",\n" +
			"                \"Sunday Night\",\n" +
			"                \"Monday\"\n" +
			"            ],\n" +
			"            \"startValidTime\":[\n" +
			"                \"2014-04-15T12:00:00-04:00\",\n" +
			"                \"2014-04-15T18:00:00-04:00\",\n" +
			"                \"2014-04-16T06:00:00-04:00\",\n" +
			"                \"2014-04-16T18:00:00-04:00\",\n" +
			"                \"2014-04-17T06:00:00-04:00\",\n" +
			"                \"2014-04-17T18:00:00-04:00\",\n" +
			"                \"2014-04-18T06:00:00-04:00\",\n" +
			"                \"2014-04-18T18:00:00-04:00\",\n" +
			"                \"2014-04-19T06:00:00-04:00\",\n" +
			"                \"2014-04-19T18:00:00-04:00\",\n" +
			"                \"2014-04-20T06:00:00-04:00\",\n" +
			"                \"2014-04-20T18:00:00-04:00\",\n" +
			"                \"2014-04-21T06:00:00-04:00\"\n" +
			"            ],\n" +
			"            \"tempLabel\":[\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\"]\n" +
			"        },\n" +
			"        \"data\":{\n" +
			"            \"temperature\":[\"69\",\"30\",\"48\",\"35\",\"55\",\"36\",\"57\",\"42\",\"60\",\"43\",\"61\",\"44\",\"62\"],\n" +
			"            \"pop\":[\"100\",\"90\",null,null,null,null,null,null,\"40\",null,null,null,\"30\"],\n" +
			"\n" +
			"            \"weather\":[\n" +
			"                \"Heavy Rain\",\n" +
			"                \"Rain/Snow\",\n" +
			"                \"Breezy\",\n" +
			"                \"Mostly Clear\",\n" +
			"                \"Sunny\",\n" +
			"                \"Partly Cloudy\",\n" +
			"                \"Partly Sunny\",\n" +
			"                \"Mostly Cloudy\",\n" +
			"                \"Chance Showers\",\n" +
			"                \"Mostly Cloudy\",\n" +
			"                \"Mostly Sunny\",\n" +
			"                \"Partly Cloudy\",\n" +
			"                \"Chance Showers\"\n" +
			"                ],\n" +
			"            \"iconLink\":[\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/shra100.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nrasn90.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/wind.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nfew.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/few.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nsct.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/bkn.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nbkn.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/shra40.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nbkn.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/sct.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/nsct.png\",\n" +
			"                \"http://forecast.weather.gov/images/wtf/medium/shra30.png\"\n" +
			"                ],\n" +
			"            \"hazard\":[\n" +
			"                \"Freeze Warning\",\t\t\t\"Hazardous Weather Outlook\",\t\t\t\"Short Term Forecast\"\n" +
			"                ],\n" +
			"            \"hazardUrl\":[\n" +
			"                \"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Freeze+Warning\",\t\t\t\"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Hazardous+Weather+Outlook\",\t\t\t\"http://forecast.weather.gov/showsigwx.php?warnzone=PAZ071&amp;warncounty=PAC101&amp;firewxzone=PAZ071&amp;local_place1=Philadelphia+International+Airport+PA&amp;product1=Short+Term+Forecast\"\n" +
			"                ],\n" +
			"            \"text\":[\n" +
			"                \"Showers and possibly a thunderstorm. Some of the storms could produce heavy rainfall.  High near 69. Breezy, with a southwest wind around 21 mph, with gusts as high as 31 mph.  Chance of precipitation is 100%. New rainfall amounts between a half and three quarters of an inch possible. \",\n" +
			"                \"Rain showers, possibly mixing with snow after 1am, then gradually ending. Some thunder is also possible.  Low around 30. Blustery, with a northwest wind 21 to 24 mph, with gusts as high as 40 mph.  Chance of precipitation is 90%. Little or no snow accumulation expected. \",\n" +
			"                \"Sunny, with a high near 48. Breezy, with a north wind 13 to 21 mph. \",\n" +
			"                \"Mostly clear, with a low around 35. Northeast wind 6 to 8 mph. \",\n" +
			"                \"Sunny, with a high near 55. East wind around 8 mph. \",\n" +
			"                \"Partly cloudy, with a low around 36.\",\n" +
			"                \"Partly sunny, with a high near 57.\",\n" +
			"                \"Mostly cloudy, with a low around 42.\",\n" +
			"                \"A chance of showers.  Mostly cloudy, with a high near 60. Chance of precipitation is 40%.\",\n" +
			"                \"Mostly cloudy, with a low around 43.\",\n" +
			"                \"Mostly sunny, with a high near 61.\",\n" +
			"                \"Partly cloudy, with a low around 44.\",\n" +
			"                \"A chance of showers.  Partly sunny, with a high near 62. Chance of precipitation is 30%.\"\t\t\t]\n" +
			"            },\n" +
			"            \"currentobservation\":{\n" +
			"                \"id\":\"KPHL\",\n" +
			"                \"name\":\"Philadelphia International Airport\",\n" +
			"                \"elev\":\"5\",\n" +
			"                \"latitude\":\"39.87\",\n" +
			"                \"longitude\":\"-75.23\",\n" +
			"                \"Date\":\"15 Apr 11:54 am EDT\",\n" +
			"                \"Temp\":\"66\",\n" +
			"                \"Dewp\":\"61\",\n" +
			"                \"Relh\":\"84\",\n" +
			"                \"Winds\":\"14\",\n" +
			"                \"Windd\":\"170\",\n" +
			"                \"Gust\":\"0\",\n" +
			"                \"Weather\":\" Light Rain\",\n" +
			"                \"Weatherimage\":\"ra.png\",\n" +
			"                \"Visibility\":\"5.00\",\n" +
			"                \"Altimeter\":\"1001.7\",\n" +
			"                \"SLP\":\"29.58\",\n" +
			"                \"timezone\":\"EDT\",\n" +
			"                \"state\":\"PA\",\n" +
			"                \"WindChill\":\"66\"\n" +
			"            }\n" +
			"        }\n";

	String test3 = "{\n" +
			"\t\"operationalMode\":\"Production\",\n" +
			"\t\"srsName\":\"WGS 1984\",\n" +
			"\t\"creationDate\":\"2014-07-01T06:02:43-09:00\",\n" +
			"\t\"creationDateLocal\":\"1 Jul 05:58 am HST\",\n" +
			"\t\"productionCenter\":\"Honolulu, HI\",\n" +
			"\t\"credit\":\"http://www.prh.noaa.gov/hnl/\",\n" +
			"\t\"moreInformation\":\"http://weather.gov\",\n" +
			"\t\"location\":{\n" +
			"\t\t\"latitude\":\"21.45\",\n" +
			"\t\t\"longitude\":\"-158.18\",\n" +
			"\t\t\"elevation\":\"131\",\n" +
			"\t\t\"wfo\":\"HFO\",\n" +
			"\t\t\"timezone\":\"H|N|10\",\n" +
			"\t\t\"areaDescription\":\"Waianae HI\",\n" +
			"\t\t\"radar\":\"phmo\",\n" +
			"\t\t\"zone\":\"HIZ006\",\n" +
			"\t\t\"county\":\"HIC003\",\n" +
			"\t\t\"firezone\":\"HIZ006\",\n" +
			"\t\t\"metar\":\"PHHI\"\n" +
			"},\n" +
			"\t\"time\":{\n" +
			"\t\t\"layoutKey\":\"k-p12h-n13-1\",\n" +
			"\t\t\"startPeriodName\":[\n" +
			"\t\t\t\"Today\",\n" +
			"\t\t\t\"Tonight\",\n" +
			"\t\t\t\"Wednesday\",\n" +
			"\t\t\t\"Wednesday Night\",\n" +
			"\t\t\t\"Thursday\",\n" +
			"\t\t\t\"Thursday Night\",\n" +
			"\t\t\t\"Independence Day\",\n" +
			"\t\t\t\"Friday Night\",\n" +
			"\t\t\t\"Saturday\",\n" +
			"\t\t\t\"Saturday Night\",\n" +
			"\t\t\t\"Sunday\",\n" +
			"\t\t\t\"Sunday Night\",\n" +
			"\t\t\t\"Monday\"\t\t\n" +
			"\t\t],\n" +
			"\t\t\"startValidTime\":[\n" +
			"\t\t\t\"2014-07-01T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-01T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-02T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-02T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-03T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-03T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-04T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-04T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-05T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-05T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-06T06:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-06T18:00:00-09:00\",\n" +
			"\t\t\t\"2014-07-07T06:00:00-09:00\"\t\n" +
			"\t\t],\n" +
			"\t\t\"tempLabel\":[\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\",\"Low\",\"High\"]\n" +
			"\t},\n" +
			"\t\"data\":{\n" +
			"\t\t\"temperature\":[\"87\",\"67\",\"87\",\"68\",\"86\",\"68\",\"86\",\"68\",\"86\",\"68\",\"86\",\"68\",\"86\"],\t\t\n" +
			"\t\t\"pop\":[\"20\",null,null,null,null,null,null,null,null,null,null,null,null],\n" +
			"\n" +
			"\t\t\"weather\":[\n" +
			"\t\t\t\"Isolated Showers\",\n" +
			"\t\t\t\"Mostly Clear\",\n" +
			"\t\t\t\"Sunny\",\n" +
			"\t\t\t\"Mostly Clear\",\n" +
			"\t\t\t\"Sunny\",\n" +
			"\t\t\t\"Mostly Clear\",\n" +
			"\t\t\t\"Sunny\",\n" +
			"\t\t\t\"Clear\",\n" +
			"\t\t\t\"Sunny\",\n" +
			"\t\t\t\"Clear\",\n" +
			"\t\t\t\"Sunny\",\n" +
			"\t\t\t\"Clear\",\n" +
			"\t\t\t\"Sunny\"\n" +
			"\t\t\t],\n" +
			"\t\t\"iconLink\":[\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/hi_shwrs20.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nfew.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/few.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nfew.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/few.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nfew.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/skc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nskc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/skc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nskc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/skc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/nskc.png\",\n" +
			"\t\t\t\"http://forecast.weather.gov/images/wtf/medium/skc.png\"\n" +
			"\t\t\t],\n" +
			"\t\t\"hazard\":[\n" +
			"\n" +
			"\t\t\t],\n" +
			"\t\t\"hazardUrl\":[\n" +
			"\n" +
			"\t\t\t],\n" +
			"\t\t\"text\":[\n" +
			"\t\t\t\"Isolated showers after noon.  Mostly sunny, with a high near 87. East wind 8 to 13 mph becoming southwest in the morning.  Chance of precipitation is 20%.\",\n" +
			"\t\t\t\"Mostly clear, with a low around 67. East wind around 9 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 87. East wind 9 to 11 mph. \",\n" +
			"\t\t\t\"Mostly clear, with a low around 68. East wind around 11 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 86. East wind around 10 mph. \",\n" +
			"\t\t\t\"Mostly clear, with a low around 68. East wind around 11 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 86. Southeast wind around 11 mph. \",\n" +
			"\t\t\t\"Clear, with a low around 68. East wind around 11 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 86. East wind 11 to 14 mph, with gusts as high as 20 mph. \",\n" +
			"\t\t\t\"Clear, with a low around 68. East wind around 14 mph, with gusts as high as 20 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 86. East wind 10 to 13 mph, with gusts as high as 18 mph. \",\n" +
			"\t\t\t\"Clear, with a low around 68. East wind around 11 mph. \",\n" +
			"\t\t\t\"Sunny, with a high near 86. East wind around 11 mph. \"\t\t\t]\n" +
			"\t\t},\n" +
			"\t\t\"currentobservation\":{\n" +
			"\t\t\t\"id\":\"PHHI\",\n" +
			"\t\t\t\"name\":\"Wheeler Army Airfield\",\n" +
			"\t\t\t\"elev\":\"827\",\n" +
			"\t\t\t\"latitude\":\"21.48\",\n" +
			"\t\t\t\"longitude\":\"-158.03\",\n" +
			"\t\t\t\"Date\":\"1 Jul 05:58 am HST\",\n" +
			"\t\t\t\"Temp\":\"69\",\n" +
			"\t\t\t\"Dewp\":\"65\",\n" +
			"\t\t\t\"Relh\":\"86\",\n" +
			"\t\t\t\"Winds\":\"2\",\n" +
			"\t\t\t\"Windd\":\"230\",\n" +
			"\t\t\t\"Gust\":\"0\",\n" +
			"\t\t\t\"Weather\":\"Mostly Cloudy\",\n" +
			"\t\t\t\"Weatherimage\":\"bkn.png\",\n" +
			"\t\t\t\"Visibility\":\"10.00\",\n" +
			"\t\t\t\"Altimeter\":\"1014.6\",\n" +
			"\t\t\t\"SLP\":\"29.99\",\n" +
			"\t\t\t\"timezone\":\"HST\",\n" +
			"\t\t\t\"state\":\"HI\",\n" +
			"\t\t\t\"WindChill\":\"72\"\n" +
			"\t\t}\n" +
			"\t}\n" +
			"\t";

}
