package net.labonski.NOAAWeather.forecast;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import net.labonski.NOAAWeather.R;

/**
 * Created by jlabonski on 6/23/2014.
 */
public class ForecastFragment extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.forecast_fragment, container, false);
    }
}