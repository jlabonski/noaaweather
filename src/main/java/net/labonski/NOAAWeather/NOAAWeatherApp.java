package net.labonski.NOAAWeather;

import android.app.Application;
import android.content.Context;

/**
 * Created by jlabonski on 4/11/2014.
 */
public class NOAAWeatherApp extends Application
{
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext(){
        return context;
    }
}
