package net.labonski.NOAAWeather.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class JodaDateTimeDeserializer implements JsonDeserializer<DateTime>
{

    public DateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException
    {

        DateTime dt = null;
	    String dateString = json.getAsJsonPrimitive().getAsString();

	    try{
		    dt = new DateTime(dateString);
		    return dt;
	    }catch(Exception e){ /* do nothing */ }

	    // Ok, obviously not in ISO8601 or anything else rational, let's try the custom handler.
	    // We'll throw if this doesn't work.

	    // WHY DO THIS, NOAA? DO YOU HATE GOOD THINGS?
		// 24 Jun 10:54 am EDT
	    DateTimeFormatter dtf = DateTimeFormat.forPattern("d MMM hh:mm a z");
	    dt = DateTime.parse(dateString, dtf);
	    dt = dt.year().setCopy(DateTime.now().year().get());
		return dt;

    }

}
