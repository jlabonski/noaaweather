package net.labonski.NOAAWeather.gson;

import com.google.gson.*;
import net.labonski.NOAAWeather.model.Forecast;
import net.labonski.NOAAWeather.model.ForecastPeriod;
import net.labonski.NOAAWeather.model.Hazard;
import org.joda.time.DateTime;

import java.lang.reflect.Type;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class ForecastDeserializer implements JsonDeserializer<Forecast>
{
    public Forecast deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException
    {
	    // Need a vanilla gson deserializer to attempt first pass
	    GsonBuilder gsonBuilder = new GsonBuilder();
	    gsonBuilder.registerTypeAdapter(DateTime.class, new JodaDateTimeDeserializer());
	    Gson gson = gsonBuilder.create();

	    Forecast f = gson.fromJson(json, Forecast.class);

	    // Trim all of the strings
	    cleanForecastStrings(f);
	    cleanLocationStrings(f);
	    cleanObservationStrings(f);

	    // We want to peel out the "data" and "time" subelements and convert them into a List
	    // of ForecastPeriods, instead of parallel arrays of temps and forecasts.

	    // and away we go
	    // Peel out the high level objects, "data" and "time". Each time we peel out json data,
	    // do some quick sanity checks on null and array length.

	    JsonObject root = null;
	    JsonObject data = null;
	    JsonObject time = null;

	    try{
		    root = json.getAsJsonObject();
		    data = root.getAsJsonObject("data");
		    time = root.getAsJsonObject("time");
	    }catch(Exception e){
		    throw new JsonParseException("Unable to parse data or time element from MapClick JSON");
	    }
	    if(data == null || time == null){
		    throw new JsonParseException("Unable to parse data element from MapClick JSON");
	    }

	    // Now the big array.
	    JsonArray periodNames  = null;
	    JsonArray periodTimes  = null;
	    JsonArray tempLabels   = null;
	    JsonArray temperatures = null;
	    JsonArray pop          = null;
	    JsonArray weather      = null;
	    JsonArray icons        = null;
	    JsonArray text         = null;

	    try{
		    periodNames  = time.getAsJsonArray("startPeriodName");
		    periodTimes  = time.getAsJsonArray("startValidTime");
		    tempLabels   = time.getAsJsonArray("tempLabel");
		    temperatures = data.getAsJsonArray("temperature");
		    pop          = data.getAsJsonArray("pop");
		    weather      = data.getAsJsonArray("weather");
		    icons        = data.getAsJsonArray("iconLink");
		    text         = data.getAsJsonArray("text");
	    }catch(Exception e){
		    throw new JsonParseException("Unable to parse subelements from MapClick JSON");
	    }

	    if(periodNames  == null ||
		   periodTimes  == null ||
		   tempLabels   == null ||
		   temperatures == null ||
		   pop          == null ||
		   weather      == null ||
		   icons        == null ||
		   text         == null){
		    throw new JsonParseException("Unable to parse subelements from MapClick JSON, may be null");
	    }


	    // All should have the same array size.
	    if(periodNames.size()  != periodTimes.size()  ||
		   periodTimes.size()  != tempLabels.size()   ||
		   tempLabels.size()   != temperatures.size() ||
		   temperatures.size() != pop.size()          ||
	       pop.size()          != weather.size()      ||
		   weather.size()      != icons.size()        ||
		   icons.size()        != text.size()){
		    throw new JsonParseException("Forecast data arrays are ragged");
	    }

		// Assign
	    for(int i = 0; i < periodNames.size(); i++){
		    ForecastPeriod p = new ForecastPeriod();

		    p.name      = periodNames.get(i).getAsString().trim();
		    p.startTime = new DateTime(periodTimes.get(i).getAsJsonPrimitive().getAsString());
		    p.tempLabel = tempLabels.get(i).getAsString().trim();
		    p.temp      = temperatures.get(i).getAsInt();
		    p.pop       = pop.get(i).isJsonNull() ? 0 : pop.get(i).getAsInt();
		    p.weather   = weather.get(i).getAsString().trim();
		    p.icon      = icons.get(i).getAsString().trim();
		    p.text      = text.get(i).getAsString().trim();

		    f.periods.add(p);

	    }


	    // hazard data may be null, don't worry about it too much.
	    // If there's any kind of data malformance or nulls, we'll just catch all
	    // and go our merry way.
		try{
			JsonArray hazard     = null;
			JsonArray hazardUrls = null;

			hazard = data.getAsJsonArray("hazard");
			hazardUrls = data.getAsJsonArray("hazardUrl");

			for(int i = 0; i < hazard.size(); i++){
				Hazard h = new Hazard();
				h.type = hazard.get(i).getAsString().trim();
				h.url  = hazardUrls.get(i).getAsString().trim();
				f.hazards.add(h);
			}
		}catch(Exception e){ /* Do nothing */}


        return f;
    }

	private void cleanLocationStrings(Forecast f)
	{
		f.location.areaDescription = f.location.areaDescription.trim();
		f.location.county = f.location.county.trim();
		f.location.firezone = f.location.firezone.trim();
		f.location.metar = f.location.metar.trim();
		f.location.radar = f.location.radar.trim();
		f.location.timezone = f.location.timezone.trim();
		f.location.wfo = f.location.wfo.trim();
		f.location.zone = f.location.zone.trim();
	}

	private void cleanForecastStrings(Forecast f)
	{
		f.credit = f.credit.trim();
		f.moreInformation = f.moreInformation.trim();
		f.productionCenter = f.productionCenter.trim();
	}

	private void cleanObservationStrings(Forecast f)
	{
		f.observation.id = f.observation.id.trim();
		f.observation.name = f.observation.name.trim();
		f.observation.state = f.observation.state.trim();
		f.observation.timezone = f.observation.timezone.trim();
		f.observation.weather = f.observation.weather.trim();
		f.observation.weatherImage = f.observation.weatherImage.trim();
	}

}
