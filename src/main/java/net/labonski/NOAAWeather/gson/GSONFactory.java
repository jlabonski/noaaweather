package net.labonski.NOAAWeather.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.labonski.NOAAWeather.model.Forecast;
import org.joda.time.DateTime;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class GSONFactory
{
    private static GsonBuilder builder;

    private GSONFactory()
    {}


    public static synchronized Gson get()
    {
        if(builder == null){
            builder = new GsonBuilder();
            registerTypes();
        }

        return builder.create();
    }

    /**
     * Registers custom gson type deserializers.
     */
    private static void registerTypes()
    {
        builder.registerTypeAdapter(DateTime.class, new JodaDateTimeDeserializer());
	    builder.registerTypeAdapter(Forecast.class, new ForecastDeserializer());
    }




}
