package net.labonski.NOAAWeather.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class Observation
{
    public String id;
    public String name;
    @SerializedName("elev") public int elevation;
    public float latitude;
    public float longitude;
	@SerializedName("Date") public DateTime date;
	@SerializedName("Temp") public int temp;
    @SerializedName("Dewp") public int dewpoint;
    @SerializedName("Relh") public int relativeHumidity;
    @SerializedName("Winds") public int windSpeed;
    @SerializedName("Windd") public int windDirection;
	@SerializedName("Gust") public int gust;
	@SerializedName("Weather") public String weather;
	@SerializedName("Weatherimage") public String weatherImage;
	@SerializedName("Visibility") public float visibility;
	@SerializedName("Altimeter") public float altimeter;
    public float SLP;
	public String timezone;
    public String state;
	@SerializedName("WindChill") public int windchill;

}