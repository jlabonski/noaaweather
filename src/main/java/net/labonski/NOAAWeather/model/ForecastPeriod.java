package net.labonski.NOAAWeather.model;

import org.joda.time.DateTime;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class ForecastPeriod
{
    public String name;
    public DateTime startTime;
    public String tempLabel;
    public int temp;
    public int pop;
    public String weather;
    public String icon;
    public String text;
}
