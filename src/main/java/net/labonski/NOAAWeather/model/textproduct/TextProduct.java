package net.labonski.NOAAWeather.model.textproduct;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;

import java.util.*;

/**
 * Encapulates a Text based product from NOAA. These are a kinda-sorta machine parsable,
 * human readable format that has strong roots in the mists of teletype.
 *
 * CAUTION: HERE THAR BE SARPENTS
 * This format is hilariously poorly designed. Much more "human readable with some embedded codes"
 * than a proper "format" or anything like that. NOAA doesn't do rational or sane date representation,
 * so there's significant jiggery-pokery involved for normalization. Lots of formats blindly ignore
 * their own specifications, lots of specifications aren't up to date. Expect random delimiters,
 * extra/no whitespace, new segments, broken headers, etc, etc, etc.
 *
 * They also hate you. You, specifically. And they won't tell you why.
 */
abstract class TextProduct
{
	public String       dataTypeLocation;
	public String       issuingOffice;
	public DateTime     issuance;
	public DateTimeZone timezone;
	public String       productShortName;
	public String       originatingOffice;
	public List<String> BroadcastInstructions = new ArrayList<String>();
	public String       productLongName;
	public String       fieldOffice;

	protected String   raw;
	protected String[] lines;
	protected String[] header;
	protected String[] data;

	// We hope this is correct
	private final int HEADER_LINES = 6;

	// NOAA 10-1701, 4.2.1
	private final String[] bulletins = {"FLASH_-_", "BULLETIN_-_", "URGENT_-_", "REGULAR_-_", "HOLD_-_"};

	// NOAA 10-1701, 3.10 (let's convert us some of them there international zones to real 'Merican zones)
	private Map<String, DateTimeZone> jodaNOAATimeZoneNames = new HashMap<String, DateTimeZone>()
	{{
			put("UTC", DateTimeZone.UTC);
			put("AST", DateTimeZone.forID("America/Puerto_Rico"));
			put("EST", DateTimeZone.forID("America/New_York"));
			put("EDT", DateTimeZone.forID("America/New_York"));
			put("CST", DateTimeZone.forID("America/Chicago"));
			put("CDT", DateTimeZone.forID("America/Chicago"));
			put("MST", DateTimeZone.forID("America/Denver"));
			put("MDT", DateTimeZone.forID("America/Denver"));
			put("PST", DateTimeZone.forID("America/Los_Angeles"));
			put("PDT", DateTimeZone.forID("America/Los_Angeles"));
			put("AST", DateTimeZone.forID("America/Anchorage"));
			put("ADT", DateTimeZone.forID("America/Anchorage"));
			put("AKST", DateTimeZone.forID("America/Anchorage"));
			put("AKDT", DateTimeZone.forID("America/Anchorage"));
			put("HST", DateTimeZone.forID("Pacific/Honolulu"));
			put("SST", DateTimeZone.forID("Pacific/Samoa"));
			put("CHST", DateTimeZone.forID("Pacific/Guam"));
			put("GUAM LST", DateTimeZone.forID("Pacific/Guam"));
		}};


	protected void parse(String raw) throws TextProductParseException
	{
		if(raw == null)
			throw new TextProductParseException("Unable to parse, data passed is null");

		if(raw.length() == 0)
			throw new TextProductParseException("Unable to parse string of length 0");

		// Cleanup, split into lines
		this.raw = cleanRaw(raw);
		this.lines = this.raw.split("\\n");

		if(lines.length < HEADER_LINES)
			throw new TextProductParseException("Document is too small; cannot contain header");

		// split off header from data
		header = Arrays.copyOfRange(lines, 0, HEADER_LINES);
		data = Arrays.copyOfRange(lines, HEADER_LINES, lines.length);

		// Zap any empty lines (padding) in data, so that we give a clean
		// set to subclass parsers.
		while(data[0].trim().length() == 0)
			data = Arrays.copyOfRange(data, 1, data.length);

		parseHeader();
		parseSegments();

	}

	/**
	 * Cleans up the raw text product. NOAA data dissemination is <i>messy</i>, and doesn't adhere
	 * well to their own standards. Be on guard for random additional strings and whatnot.
	 *
	 * @param raw
	 * @return A cleaned up raw string.
	 */
	protected String cleanRaw(String raw)
	{
		raw = raw.trim();

		raw = raw.replaceFirst("^\\n+", "");

		// This shows up from getProdDBQuery, and isn't defined under NOAA 10-1701, Text Product
		// Formats and Code.
		//
		// No idea. Remove it.
		raw = raw.replaceFirst("^000\\n", "");

		// Strip "Start of Message Code", 10-1701, 4.1.1
		// This may vary!
		raw = raw.replaceFirst("^##\\n", "");

		return raw;
	}

	/**
	 * Parses out the Text Product Header.
	 *
	 * See NOAA 10-1701, http://www.nws.noaa.gov/directives/
	 *
	 * @throws TextProductParseException
	 */
	protected void parseHeader() throws TextProductParseException
	{

		/* FIXME: this is incomplete!
		 * This needs to support multiple, optional lines. What really should happen
		 * is that the raw lines get "digested" one by one by a state machine, leaving the data
		 * behind for subclass handling.
		 *
		 * I hate writing those. :) I'll implement later; at least the AFD implementation
		 * seems to not have any of the gotchas present
		 */



		/* FXUS61 KPHI 301626
		 * AFDPHI
		 *
		 * AREA FORECAST DISCUSSION
		 * NATIONAL WEATHER SERVICE MOUNT HOLLY NJ
		 * 1226 PM EDT MON JUN 30 2014
		 */
		final int WMO_ABBREVIATED_HEADING = 0;
		final int AIWPS_IDENTIFIER = 1;
		final int PRODUCT_TYPE = 3;
		final int ISSUING_OFFICE = 4;
		final int ISSUANCE_DATETIME = 5;

		try{
			String[] bits = header[WMO_ABBREVIATED_HEADING].split(" ");
			dataTypeLocation = bits[0];
			issuingOffice = bits[1];

			productShortName = header[AIWPS_IDENTIFIER].substring(0, 3);
			originatingOffice = header[AIWPS_IDENTIFIER].substring(3, header[AIWPS_IDENTIFIER].length());

			// FIXME: implement Broadcast Instruction line, 4.2.1

			productLongName = header[PRODUCT_TYPE];

			// FIXME: implement multiple issuing offices, 4.2.3.1

			fieldOffice = header[ISSUING_OFFICE];

			// time is formatted in a stupid, stupid way. Let's pretend that ISO8601
			// doesn't exist, and we want to make it as hard as possible, shall we?
			//
			// Two big problems: time may be three or four digits long (derp), and
			// timezones aren't standard. Pad the string with a leading 0 to make sure
			// we can parse single digit hours.

			String[] chunks = header[ISSUANCE_DATETIME].split("\\s+");
			if(chunks[0].length() == 3){
				header[ISSUANCE_DATETIME] = "0" + header[ISSUANCE_DATETIME];
			}

			// 1226 PM EDT MON JUN 30 2014
			// 334 AM HST TUE JUL 1 2014
			// THIS IS WHY WE CAN'T HAVE NICE THINGS, NOAA. THIS IS WHY.
			DateTimeFormatter dtf = new DateTimeFormatterBuilder().appendClockhourOfHalfday(1)
					.appendMinuteOfHour(2)
					.appendLiteral(' ')
					.appendHalfdayOfDayText()
					.appendLiteral(' ')
					.appendTimeZoneName(jodaNOAATimeZoneNames)
					.appendLiteral(' ')
					.appendDayOfWeekShortText()
					.appendLiteral(' ')
					.appendMonthOfYearShortText()
					.appendLiteral(' ')
					.appendDayOfMonth(1)
					.appendLiteral(' ')
					.appendYear(4, 4)
					.toFormatter();

			issuance = DateTime.parse(header[ISSUANCE_DATETIME], dtf);
			timezone = issuance.getZone();

		}catch(Exception e){
			throw new TextProductParseException("Failed to parse header", e);
		}
	}

	protected abstract void parseSegments() throws TextProductParseException;
}
