package net.labonski.NOAAWeather.model.textproduct;

/**
 * Created by jlabonski on 7/1/2014.
 */
public class TextProductParseException extends Exception
{

	public TextProductParseException(String message)
	{
		super(message);
	}

	public TextProductParseException(String message, Throwable throwable)
	{
		super(message, throwable);
	}
}
