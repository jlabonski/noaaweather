package net.labonski.NOAAWeather.model.textproduct;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Encapsulates the Area Forecast Discussion product
 *
 * Created by jlabonski on 6/30/2014.
 */
public class ForecastDiscussion extends TextProduct
{
	public Map<String, String> segments = new LinkedHashMap<String, String>();

	public String synopsis;
	public String nearTerm;
	public String shortTerm;
	public String longTerm;
	public String aviation;
	public String marine;
	public String fireWeather;
	public String hydrology;
	public String ripCurrents;
	public String discussion;


	public void parse(String raw) throws TextProductParseException
	{
		super.parse(raw);
	}


	@Override
	protected void parseSegments() throws TextProductParseException
	{


		StringBuilder sb = new StringBuilder();
		for(String line : data){
			sb.append(line).append("\n");
		}

		String foo = sb.toString();
		String[] chunks = foo.split("\\n+&&\\n+");

		for(String chunk : chunks){
			if(chunk.startsWith(".SYNOPSIS...")){
				synopsis = cleanSegment(chunk);
			}else if(chunk.startsWith(".NEAR TERM")){
				nearTerm = cleanSegment(chunk);
			}else if(chunk.startsWith(".SHORT TERM")){
				shortTerm = cleanSegment(chunk);
			}else if(chunk.startsWith(".LONG TERM")){
				longTerm = cleanSegment(chunk);
			}else if(chunk.startsWith(".AVIATION")){
				aviation = cleanSegment(chunk);
			}else if(chunk.startsWith(".MARINE")){
				marine = cleanSegment(chunk);
			}else if(chunk.startsWith(".FIRE WEATHER")){
				fireWeather = cleanSegment(chunk);
			}else if(chunk.startsWith(".HYDROLOGY")){
				hydrology = cleanSegment(chunk);
			}else if(chunk.startsWith(".RIP CURRENTS")){
				ripCurrents = cleanSegment(chunk);
			}else if(chunk.startsWith(".HYDROLOGY")){
				hydrology = cleanSegment(chunk);
			}else if(chunk.startsWith(".DISCUSSION")){
				discussion = cleanSegment(chunk);
			}
		}
	}

	// This could be better and extract the time frame from the segment header
	private String cleanSegment(String segment) throws TextProductParseException
	{
		// Eat up to the first newline, reformat the 80-char-terminal newlines into spaces for modern screens.
		int newline = segment.indexOf("\n");
		return segment.substring(newline + 1, segment.length()).replace('\n', ' ');
	}


}
