package net.labonski.NOAAWeather.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class Forecast
{
	public DateTime creationDate;
	public String   credit;
	public String   productionCenter;
	public String   moreInformation;

	public Location location;
	public List<ForecastPeriod> periods  = new ArrayList<ForecastPeriod>();
	public List<Hazard>         hazards  = new ArrayList<Hazard>();

	@SerializedName("currentobservation")
	public Observation observation;

}
