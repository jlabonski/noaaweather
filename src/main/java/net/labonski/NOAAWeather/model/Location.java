package net.labonski.NOAAWeather.model;

/**
 * Created by jlabonski on 6/24/2014.
 */
public class Location
{
    public float latitude;
    public float longitude;
    public int elevation; // meters?
    public String wfo;
    public String timezone;
    public String areaDescription;
    public String radar;
    public String zone;
    public String county;
    public String firezone;
    public String metar;

}
